import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Axios from 'axios'

// @ts-ignore
import vuetify from '@/plugins/vuetify'

//@ts-ignore
import VeeValidate, { Validator } from 'vee-validate'

//@ts-ignore
import { VueMaskDirective } from 'v-mask'

//@ts-ignore
import ru from 'vee-validate/dist/locale/ru'
const vvconfig = {
  fieldsBagName: [], // change if property conflicts.
}

Vue.use(VeeValidate, vvconfig)
Validator.localize('ru', ru);
Vue.use(vuetify)

Vue.directive('mask', VueMaskDirective);

Vue.config.productionTip = false;

Vue.prototype.$checkMobileBrowser = () => {
  let check = false;
  (function(a) {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(navigator.userAgent);
  return check;
}

const phoneRule = {
  getMessage(field: any, args: any) {
    return `Введите валидный номер телефон`;
  },
  validate(value: any, args: any) {
    const formatted_value = value.replaceAll('(', '').replaceAll(')', '').replaceAll(' ', '').replaceAll('-', '')
    // Custom regex for a phone number 
    const MOBILEREG = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    // Check for either of these to return true
    return MOBILEREG.test(formatted_value);
  }
};

//@ts-ignore
import { Validator } from 'vee-validate';

//@ts-ignore
import PhoneNumber from 'awesome-phonenumber'

const phoneNumber = {
  getMessage: (field: any) => `${field} is not a valid phone number`,
  validate (value: any) {
    return new Promise(resolve => {
      const formatted_value = value.replaceAll('(', '').replaceAll(')', '').replaceAll(' ', '').replaceAll('-', '')
      console.log('value', `8${formatted_value}`)
      let phone = new PhoneNumber(`8${formatted_value}`);
      resolve({ valid: phone.isValid() })
    })
  }
}
//@ts-ignore
Validator.extend('phoneNumber', phoneRule)

export const axios = Axios.create({
  baseURL: process.env.VUE_APP_STRAPI_DEVELOP,
  timeout: 30000,
  headers: { 'Content-Type': 'application/json' }
})

new Vue({
  router,
  store,
  //@ts-ignore
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
