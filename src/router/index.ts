import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/MeetSection.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Home',
    meta: {
      title: 'Главная'
    },
    component: Home,
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.afterEach((to, from) => {
  Vue.nextTick(() => {
      document.title = `${to.meta.title} - ${document.title}`;
  });
});

export default router;
