import { Module, VuexModule, getModule, Mutation, Action } from 'vuex-module-decorators'
import store from '@/store'
import { axios } from '@/main'

@Module({
  namespaced: true,
  name: 'user',
  store,
  dynamic: true
})
class User extends VuexModule {
  
}

export default getModule(User)
